using GraphQL.Types;
using BingoGraphQL.Models;
using BingoGraphQL.Repositories;

namespace BingoGraphQL.GraphQl.Types
{
    class AdminType : ObjectGraphType<Administrator>
    {
        public AdminType(AdminRepository adminRepository){

            Name = "Admin";
            Field(x => x.Id);
            Field(x => x.FirstName);
            Field(x => x.LastName);
            Field(x => x.Email);
            Field(x => x.PasswordHash);

        }
    }
}