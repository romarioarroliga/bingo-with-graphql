using GraphQL.Types;
using BingoGraphQL.Models;

namespace  BingoGraphQL.GraphQl.Types
{
    class BoardInputType : InputObjectGraphType
    {
        public BoardInputType()
        {
            Name = "BoardInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nameBoard");
            Field<NonNullGraphType<StringGraphType>>("url");
            Field<NonNullGraphType<StringGraphType>>("AdminUrl");
            Field<NonNullGraphType<IntGraphType>>("PlayersNumber");
            Field<ListGraphType<IntGraphType>>("GettedNumbers");
            Field<NonNullGraphType<BooleanGraphType>>("Finished");
        }
    }
}