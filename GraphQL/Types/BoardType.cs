using GraphQL.Types;
using BingoGraphQL.Models;
using BingoGraphQL.Repositories;

namespace BingoGraphQL.GraphQl.Types
{
    class BoardType : ObjectGraphType<Board>
    {
        public BoardType(){

            Name = "Board";
            Field(x => x.Id);
            Field(x => x.NameBoard);
            Field(x => x.Url);
            Field(x => x.AdminUrl);
            Field(x => x.PlayersNumber);
            Field(x => x.GettedNumbers);
            Field(x => x.Finished);

        }
    }
}