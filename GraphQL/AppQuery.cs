using GraphQL.Types;
using BingoGraphQL.Repositories;
using BingoGraphQL.GraphQl.Types;
using BingoGraphQL.Models;
using GraphQL;

namespace BingoGraphQL.GraphQl
{
    class AppQuery : ObjectGraphType
    {
        public AppQuery(AdminRepository adminRepository, BoardRepository boardRepository)
        {

            Field<AdminType>("GetAdmin",
                              arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AdminInputType>> { Name = "input" }),
                              resolve: context => {
                                  return adminRepository.Auth(context.GetArgument<Administrator>("input"));
                              });
            Field<ListGraphType<BoardType>>("boards",
                                             arguments: new QueryArguments(new QueryArgument<StringGraphType> { Name = "nameBoard" },
                                                                           new QueryArgument<IntGraphType> { Name = "orderBy", DefaultValue = 0}
                                             ),
                                                                            
                                             resolve: context => {
                                                 return boardRepository.All(context);
                                             });
            Field<BoardType>("board",
                              arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                              resolve: context => {
                                  return boardRepository.Find(context.GetArgument<int>("id"));
                              });

            


        }
        
    }
}