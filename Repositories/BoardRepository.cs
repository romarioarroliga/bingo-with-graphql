using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL.Types;
using BingoGraphQL.Models;
using System.Reactive.Linq;

namespace BingoGraphQL.Repositories
{
    class BoardRepository{

        private readonly DataBaseContext _context;
        
        public BoardRepository(DataBaseContext context)
        {
            _context = context;
        }

         public IEnumerable<Board> All(ResolveFieldContext<object> context){
            var results = from boards in _context.Boards select boards;
           
            if (context.HasArgument("nameBoard"))
            {
                var value = context.GetArgument<string>("nameBoard");
                results = results.Where(a => a.NameBoard.Contains(value));
            } 

            if (context.HasArgument("orderBy"))
            {
                var value = context.GetArgument<int>("orderBy");
                if(value == 0){

                    results = results.OrderBy(a=> a.Id);

                }else if(value == 1){

                    results = results.OrderBy(a=> a.NameBoard);

                }
            }
            return results;
        }

        public Board Find(int id){
            return _context.Boards.Find(id);
        }

        public async Task<Board> Add(Board board) {
           
            _context.Boards.Add(board);
            await _context.SaveChangesAsync();
            return board;
        }
        public async Task<Board> AddNumbers(int id, Board board) {
           
            board.Id = id;
            var updated = (_context.Boards.Update(board)).Entity;
            if (updated == null)
            {
                return null;
            }
            await _context.SaveChangesAsync();
            return updated;
        }

        public async Task<Board> Remove(int id) {
            var board = await _context.Boards.FindAsync(id);
            if (board == null)
            {
                return null;
            }
            _context.Boards.Remove(board);
            await _context.SaveChangesAsync();
            return board;
        }

    }
}