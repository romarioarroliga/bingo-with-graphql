using Microsoft.EntityFrameworkCore;

namespace BingoGraphQL.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
           
        }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Board> Boards { get; set; }
        
        
    }
}