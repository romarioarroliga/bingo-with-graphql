using System;
using System.Reactive.Subjects;
using System.Reactive.Linq;

namespace BingoGraphQL.Models
{
    
    public class Stream : IStream
    {
        private readonly ISubject<Board> _stream = new ReplaySubject<Board>(1);
        public Board AddStream(Board board)
        {
            _stream.OnNext(board);
            return board;
        }

        public IObservable<Board> StreamBoard()
        {
            return _stream.AsObservable();
        }
    }
}