import { Subscription } from 'apollo-angular';
import gql from 'graphql-tag';


export const GET_SUBSCRIPTION = gql`
  subscription{
    boardAdded{
        id
        nameBoard
        adminUrl
        url
        gettedNumbers
        finished
        playersNumber
    }
}
`;

