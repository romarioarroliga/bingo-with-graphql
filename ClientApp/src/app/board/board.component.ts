import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Board } from '../_models/board'
import { Card } from '../interfaces/card.interface';
import { GET_SUBSCRIPTION } from './Subscription';
import { QueryRef } from 'apollo-angular';
import { Apollo } from 'apollo-angular';
import { UPDATE_BOARD } from './mutations'
import { GET_BOARD } from './queries'
import { error } from '@angular/compiler/src/util';


interface IFeedbackEvent {

  id: number;
  nameBoard: string;
  gettedNumbers: [];

}

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  boards: Board[] = [];
  id: number;
  @Input() CurrentNumber: number;
  @Input() RecentNumbers: number[] = [];
  lastPost: Board;
  public numbers;
  public all_nums: number[];
  public gettedNumbers: number[];
  public cards: Card[];
  public counter: number;
  public showCounterForm: boolean;
  public win: boolean;
  public gameF: boolean;
  public cardsC: boolean;
  public recent1: number;
  public recent2: number;
  public recent3: number;
  public currentN: number;
  public nameBoard;
  public adminUrl;
  public url;
  public playersNumber;
  public localCards;
  public boardExist;


  constructor(private apollo: Apollo, private route: ActivatedRoute) {
    this.boardExist = true;
    this.route.params.subscribe(params => {

      this.id = Number(params['id']);
      this.getBoard(this.id);
    })
   
    this.apollo.subscribe({
      query: GET_SUBSCRIPTION,
    }).subscribe(result => {
      if (this.id == result.data['boardAdded'].id) {
        this.id = result.data['boardAdded'].id;
        this.gettedNumbers = result.data['boardAdded'].gettedNumbers;
        this.gameF = result.data['boardAdded'].finished;
        this.recent1 = this.gettedNumbers[this.gettedNumbers.length - 2];
        this.recent2 = this.gettedNumbers[this.gettedNumbers.length - 3];
        this.recent3 = this.gettedNumbers[this.gettedNumbers.length - 4];
        this.currentN = this.gettedNumbers[this.gettedNumbers.length - 1];
        this.url = result.data['boardAdded'].url;
        this.adminUrl = result.data['boardAdded'].adminUrl;
        this.nameBoard = result.data['boardAdded'].nameBoard;
        this.playersNumber = result.data['boardAdded'].playersNumber;
        this.game();
      }


    })
    this.cards = [];
    this.counter = 1;
    this.showCounterForm = true;
    this.all_nums = [];
    this.cardsC = false;

    for (let index = 1; index < 91; index++) {
      this.all_nums.push(index);

    }
    if (localStorage.getItem('cards') == null || localStorage.getItem('cards').length == 0) {
      this.localCards = false;
    } else {
      this.localCards = true;
      this.cards = JSON.parse(localStorage.getItem('cards'));
      this.win = JSON.parse(localStorage.getItem('win'));

    }


  }
  createCards() {
    if (this.localCards == false) {
      for (let index = 1; index < this.counter + 1; index++) {
        var newCard: Card = {
          'id': index,
          'numbers': this.genCardNumbers(),
          'completed': false
        }
        this.cards.push(newCard);
      }
      localStorage.setItem('cards', JSON.stringify(this.cards));
      this.showCounterForm = false;
      this.localCards = true;
    }



  }
  genCardNumbers() {
    var array: number[] = [];
    var arraySize = this.all_nums.length;
    for (let index = 0; index < 15;) {
      var random = Math.floor(Math.random() * (arraySize - 0)) + 0;
      if (!array.includes(random)) {
        array.push(random);
        index++;
      }
    }
    return array;
  }
  isGetted(num: number) {
    return this.gettedNumbers.includes(num);
  }
  isBiggerThan9(num: number) {
    if (num > 9) {
      return true
    }
    return false;
  }


  ngOnInit() {

  }
  game() {
    if (!this.gameF) {
      this.cards.forEach(card => {
        var inThisCard: number[] = [];
        card.numbers.forEach(number => {
          if (this.gettedNumbers.includes(number)) {
            inThisCard.push(number);
          }
        });
        if (inThisCard.length == 15) {
          this.win = true;
          localStorage.setItem('win', JSON.stringify(this.win));
          this.gameF = true;
          console.log(this.win);
          console.log(this.gameF);

          this.save(this.id);

        }
      });
    }

  }
  save(idBoard) {
    let mutation = UPDATE_BOARD;

    const variables = {
      id: idBoard,
      input: {
        id: idBoard,
        nameBoard: this.nameBoard,
        adminUrl: this.adminUrl,
        url: this.url,
        gettedNumbers: this.gettedNumbers,
        finished: this.gameF,
        playersNumber: this.playersNumber
      }

    };
    this.apollo.mutate({
      mutation: mutation,
      variables: variables
    }).subscribe(() => {

    });
  }


  getBoard(idBoard) {
    debugger
    this.apollo.watchQuery({
      query: GET_BOARD,
      fetchPolicy: 'network-only',
      variables: { id: idBoard }

    }).valueChanges.subscribe(result => {
    
        this.gettedNumbers = result.data['board'].gettedNumbers; 
        return this.boardExist = true

    }, error => (console.log(error)));

    return this.boardExist = false
  }
  closeGame() {
    localStorage.removeItem('cards');
    localStorage.removeItem('win');
    window.close();
  }
}