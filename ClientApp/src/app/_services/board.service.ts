import { Injectable } from "@angular/core";
import { BoardAdminNumber } from '../_models/boardAdminNumber'

@Injectable({
  providedIn: "root"
})
export class BoardService {

    availableNumber: Array<number> = [];
    generatedNumbers: Array<number> = [];

    BoardData: Array<Array<BoardAdminNumber>> = [];

    GenerateBoard(): Array<Array<BoardAdminNumber>> {
        this.BoardData = [];
        for (let rowIndex = 0; rowIndex < 9; rowIndex++) {
          let row = new Array<BoardAdminNumber>();
          for (let col = 1; col <= 10; col++) {
            let bNumber = new BoardAdminNumber();
            bNumber.value = rowIndex * 10 + col;   
            bNumber.selected = false;
            row.push(bNumber);     
          }   
          this.BoardData.push(row);   
        }
        return this.BoardData;
    }

    resetBoard() {
        this.availableNumber = [];
        this.generatedNumbers = [];
        for (let index = 1; index <= 90; index++) {
          this.availableNumber.push(index);      
        }
    }
    
    GenerateNextNumber(): number {
      
        if(this.availableNumber.length == 0) {
          return 0;
        }
        let min = 0;
        let max = this.availableNumber.length - 1;
        let randomIndx = this.getRandomInt(min, max);
        let value = this.availableNumber[randomIndx];
        this.UpdateBoard(value);
        this.availableNumber.splice(randomIndx, 1);
        this.generatedNumbers.push(value);
        return value;
    }

    
    UpdateBoard(value: number) {
        let row = Math.floor(value / 10);
        let col = (value % 10) - 1;
        if(value % 10 == 0) {
          row = row - 1;
          col = 9;
        } 
        this.BoardData[row][col].selected = true;
    }
      
    getRandomInt(min, max): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    GetRecentNumbers(count: number): Array<number> {
        let recentNumbers = JSON.parse(JSON.stringify(this.generatedNumbers));
        if(recentNumbers.length > count) {
          recentNumbers = recentNumbers.splice(recentNumbers.length - count, count);
        }
        return recentNumbers;
    }

      
}
