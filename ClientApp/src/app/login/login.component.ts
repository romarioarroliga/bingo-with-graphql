import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { GET_ADMIN } from './queries'
import {Apollo} from 'apollo-angular'

interface Admin {
    firstName: string;
    lastName: string;
    email :string;
    passwordHash: string;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{
  
  loginForm = new FormGroup({

      email : new FormControl('',Validators.required),
      passwordHash : new FormControl('',Validators.required)

  }) 

    
  admin: Admin;

  constructor(private apollo: Apollo, private router : Router){}
   

  onLogin(form:Admin){

    if(form.email != "" && form.passwordHash != ""){
      this.apollo.watchQuery({
          query: GET_ADMIN,
          fetchPolicy: 'network-only',
          variables: {
            input: { email: form.email, passwordHash: form.passwordHash  }
          }
      }).valueChanges.subscribe(result => {
          
          this.admin = result.data['getAdmin'];
          
          if(this.admin != null){
            localStorage.setItem('isLoggedIn', "true");  
            localStorage.setItem('token', this.admin.firstName + " " + this.admin.lastName);  
            this.router.navigate(['admin']);

          }else{
            console.log("Vacio")
          } 
         
      },error => console.log(error.error));
    }
  }
  
  
}
