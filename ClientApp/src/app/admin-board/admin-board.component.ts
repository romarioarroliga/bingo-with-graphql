import { Component, Input } from '@angular/core';
import { BoardAdminNumber } from '../_models/boardAdminNumber'
import { BoardService } from '../_services/board.service'
import { UPDATE_BOARD } from './mutations'
import { Board } from '../_models/board';
import { Apollo } from 'apollo-angular';
import { ActivatedRoute } from '@angular/router'
import { GET_BOARD } from '../admin-board/queries'

@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css']
})
export class AdminBoardComponent {
  @Input() BoardData: Array<Array<BoardAdminNumber>> = [];
  @Input() CurrentNumber: number;
  @Input() RecentNumbers: Array<number>;
  currentBoard: Board;
  gettedNumbers: number[];
  idBoard: number;
  boards: Board[] = [];
  public gameF: boolean;
  public boardExist;

  constructor(private service: BoardService, private apollo: Apollo, private route: ActivatedRoute) {
    this.BoardData = this.service.GenerateBoard();
    this.service.resetBoard();
    this.gettedNumbers = [];
    this.route.params.subscribe(params => {
      this.gameF = false;
      var id = Number(params['id']);
      this.idBoard = id;
      this.getBoard(id)

    })

  }

  NextNumber() {

    this.CurrentNumber = this.service.GenerateNextNumber();
    this.RecentNumbers = JSON.parse(JSON.stringify(this.service.GetRecentNumbers(3)));
    this.gettedNumbers.push(this.CurrentNumber);
    this.save(this.idBoard)
  }

  getBoard(idBoard) {

    this.apollo.watchQuery({
      query: GET_BOARD,
      fetchPolicy: 'network-only',
      variables: { id: idBoard }

    }).valueChanges.subscribe(result => {
      this.currentBoard = result.data['board'];
      this.gameF = result.data['board'].finished;
      this.numberInDB()
      return this.boardExist = true

    });
    return this.boardExist = false

  }
  numberInDB() {

    this.currentBoard.gettedNumbers.forEach(element => {

      if (!this.gettedNumbers.includes(element)) {
        this.gettedNumbers.push(element);
        this.service.UpdateBoard(element);
      }

    });

  }

  save(idBoard) {
    let mutation = UPDATE_BOARD;

    const variables = {
      id: idBoard,
      input: {
        id: this.currentBoard.id,
        nameBoard: this.currentBoard.nameBoard,
        adminUrl: this.currentBoard.adminUrl,
        url: this.currentBoard.url,
        gettedNumbers: this.gettedNumbers,
        finished: this.currentBoard.finished,
        playersNumber: this.currentBoard.playersNumber
      }
    };

    this.apollo.mutate({
      mutation: mutation,
      variables: variables
    }).subscribe(() => {
      this.getBoard(idBoard);
    });
  }



}
