import gql from 'graphql-tag';

 export const UPDATE_BOARD = gql`
    mutation($id: ID!, $input: BoardInput!){
        updateBoard(id: $id, input: $input){
                id
                nameBoard
        }
  
    }
`;